package com.example.zooapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zooapp.databinding.FragmentFinishBinding
import com.example.zooapp.databinding.FragmentHelloBinding

class FinishFragment : Fragment() {
    private var _binding: FragmentFinishBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFinishBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnBack2.setOnClickListener {
            val back2 = FinishFragmentDirections.actionFinishFragmentToGettingStartedFragment()
            findNavController().navigate(back2) }
        binding.btnFinish.setOnClickListener {
            val zoo_activity = FinishFragmentDirections.actionFinishFragmentToZooActivity("ZooOpolis")
            findNavController().navigate(zoo_activity)
        activity?.finish()}
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}