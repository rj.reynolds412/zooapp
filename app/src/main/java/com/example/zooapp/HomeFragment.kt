package com.example.zooapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zooapp.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentHomeBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnLion.setOnClickListener {
            val lionpage = HomeFragmentDirections.actionHomeFragmentToDetailsFragment("Lion",
                "female lions are the primary hunters")
            findNavController().navigate(lionpage)
        }
        binding.btnGorilla.setOnClickListener {
            val gorillapage = HomeFragmentDirections.actionHomeFragmentToDetailsFragment("Gorilla",
                "Gorillas are largely vegetarian")
            findNavController().navigate(gorillapage)
        }
        binding.btnGrizzley.setOnClickListener {
            val grizzleypage =
                HomeFragmentDirections.actionHomeFragmentToDetailsFragment("Grizzley Bear",
                    "Grizzley Bears are the largest terrestrial predator")
            findNavController().navigate(grizzleypage)
        }
        binding.btnJaguar.setOnClickListener {
            val jaguarpage = HomeFragmentDirections.actionHomeFragmentToDetailsFragment("Jaguar",
                "Jaguars have the strongest bite of any big cat")
            findNavController().navigate(jaguarpage)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
