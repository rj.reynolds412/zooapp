package com.example.zooapp

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.navArgument

class ZooActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zoo)
       val zooname = intent.getStringExtra("zooname")
        Toast.makeText(this, "Welcome to $zooname!!", Toast.LENGTH_SHORT).show()
    }

}