package com.example.zooapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zooapp.databinding.FragmentGettingStartedBinding

class GettingStartedFragment : Fragment() {
    private var _binding: FragmentGettingStartedBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentGettingStartedBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnBack.setOnClickListener {
            val back = GettingStartedFragmentDirections.actionGettingStartedFragmentToHelloFragment()
            findNavController().navigate(back) }
        binding.btnNext2.setOnClickListener {
            val next2 = GettingStartedFragmentDirections.actionGettingStartedFragmentToFinishFragment()
            findNavController().navigate(next2) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

